using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public GameObject TilePrefab;
    public GameObject TilePrefabB;
    //White
    public GameObject player_king;
    public GameObject player_horse;
    public GameObject player_soldier;
    public GameObject player_queen;
    public GameObject player_bishop;
    public GameObject player_tower;
    //Black
    public GameObject b_player_king;
    public GameObject b_player_horse;
    public GameObject b_player_soldier;
    public GameObject b_player_queen;
    public GameObject b_player_bishop;
    public GameObject b_player_tower;
    //Black & White
    public Material[] figures;
    public Material selected;
    public Material empty;
    public Material allowMove;
    public GameObject[,] tiles = new GameObject[8, 8];
    public GameObject currobj;
    public GameObject selectedobj;
    public GameObject emptyobj;
    public const int mapSize = 8;
	public bool enable_Castling = true;
	public bool enable_Queening = true;
    public enum moveState { start = 0, end = 1 }
    [SerializeField]
    public int[,] BoardState = new int[8, 8]   {{-4,-2,-3,-5,-6,-3,-2,-4},
                                                {-1,-1,-1,-1,-1,-1,-1,-1},
                                                { 0, 0, 0, 0, 0, 0, 0, 0},
                                                { 0, 0, 0, 0, 0, 0, 0, 0},
                                                { 0, 0, 0, 0, 0, 0, 0, 0},
                                                { 0, 0, 0, 0, 0, 0, 0, 0},
                                                { 1, 1, 1, 1, 1, 1, 1, 1},
                                                { 4, 2, 3, 5, 6, 3, 2, 4}};
    public int[,] AllowMove = new int[8, 8];
    public int selectedFigure = 0;
    [SerializeField]
    public int currentMove;
    int currentPlayerIndex = 0;
    int i_prev = 0;
    int j_prev = 0;
    public Texture newGame;
	AndroidJavaClass ajc;
	string msg;
	//ajc = new AndroidJavaClass("com.messengerchess.unityplugin");
	//public Texture endTurn;
    void Awake()
    {
        instance = this;
    }

    // Use this for initialization
    void Start()
    {
		//ajc = new AndroidJavaClass("com.messengerchess.unityplugin");
		currentMove = 1;
        SetupMap();
        UpdateFigures();
        // SetupFigures();
        // generatePlayers();
    }

    // Update is called once per frame
    void Update()
    {
        MoveFigure();

		Queening();
	}

    void OnGUI()
    {
        //for (int i = 0; i < mapSize; i++)
        //    for (int j = 0; j < mapSize; j++)
        //    {
        //        GUI.Label(new Rect(j * 201, i * 20, 20, 20), BoardState[i, j].ToString());
        //        GUI.Label(new Rect(j * 20 + 500, i * 20, 20, 20), AllowMove[i, j].ToString());
        //    }
        if (GUI.Button(new Rect(Screen.width / 2 - Screen.width / 6, Screen.height/10, Screen.width / 3, Screen.height/10), newGame))
        {
            Application.LoadLevel(Application.loadedLevel);
        }
		 
    }

    void MoveFigure()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hitInfo = new RaycastHit();
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo) && hitInfo.transform.tag == "tile")
            {
                Material[] figureMat = new Material[3];
                int j = (int)Mathf.Round(hitInfo.transform.transform.position.x);
                int i = (int)Mathf.Round(-hitInfo.transform.transform.position.z);
                if (BoardState[i, j] * currentMove > 0)
                {
                    figureMat = tiles[i, j].GetComponent<Renderer>().materials;
                    figureMat[1] = selected;
                    tiles[i, j].GetComponent<Renderer>().materials = figureMat;
                    selectedFigure = BoardState[i, j];
                    MoveFigure(i, j);
					i_prev = i;
                    j_prev = j;

                    for (int m = 0; m < mapSize; m++)
                        for (int k = 0; k < mapSize; k++)
                            if (AllowMove[m, k] == 1)
                            {
                                figureMat = tiles[m, k].GetComponent<Renderer>().materials;
                                figureMat[1] = allowMove;
                                tiles[m, k].GetComponent<Renderer>().materials = figureMat;
                            }
                            else
                            {
                                figureMat = tiles[m, k].GetComponent<Renderer>().materials;
                                figureMat[1] = empty;
                                tiles[m, k].GetComponent<Renderer>().materials = figureMat;
                            }
                }
                else if ((selectedFigure != 0) && (AllowMove[i, j] == 1))
                {
                    figureMat = tiles[i_prev, j_prev].GetComponent<Renderer>().materials;
                    figureMat[1] = empty;
                    tiles[i_prev, j_prev].GetComponent<Renderer>().materials = figureMat;
                    BoardState[i, j] = selectedFigure;
                    BoardState[i_prev, j_prev] = 0;
					selectedFigure = 0;
                    UpdateFigures();
                    nextTurn();
                    for (int m = 0; m < mapSize; m++)
                        for (int k = 0; k < mapSize; k++)
                        {
                            figureMat = tiles[m, k].GetComponent<Renderer>().materials;
                            figureMat[1] = empty;
                            tiles[m, k].GetComponent<Renderer>().materials = figureMat;
                        }
                }
            }
        }
    }

	public void Queening()
	{
		for (int i = 1; i<mapSize; i++) {
			if (BoardState[0,i] == 1)
			{
				BoardState[0,i] = 5;
				enable_Queening = false;
			}
		}
		                     
	}

    public void nextTurn()
    {
        if (currentMove == 1)
            currentMove = -1;
        else
            currentMove = 1;
    }

    void SetupMap()
    {
        for (int i = 0; i < mapSize; i++)
            for (int j = 0; j < mapSize; j++)
            {
                if ((i + j) % 2 == 1)
                {
                    tiles[i, j] = ((GameObject)Instantiate(TilePrefabB, new Vector3(j, 0, -i), Quaternion.Euler(new Vector3(0, 180, 0))));
                }
                if ((i + j) % 2 == 0)
                {
                    tiles[i, j] = ((GameObject)Instantiate(TilePrefab, new Vector3(j, 0, -i), Quaternion.Euler(new Vector3(0, 180, 0))));
                }
            }
    }

    void UpdateFigures()
    {
        Material[] figureMat = new Material[3];
        for (int i = 0; i < 8; i++)
            for (int j = 0; j < 8; j++)
            {
                figureMat = tiles[i, j].GetComponent<Renderer>().materials;//[0] = figures[BoardState[i, j] + 6];
                figureMat[2] = figures[BoardState[i, j] + 6];
                tiles[i, j].GetComponent<Renderer>().materials = figureMat;
            }
    }
	void Check()
	{
	
		for (int i = 0; i < mapSize; i++)
			for (int j = 0; j < mapSize; j++)
		{
			switch(BoardState[i,j])
			{
				//White pawn
					case 5:
	//					if (AllowMove[i,j] == 1)
	//				{
	//					if (BoardState[i - 1, j + 1] == -6)
	//						print ("check");
	//					if (BoardState[i - 1, j - 1] == -6)
	//						print ("check");
	//				}
				for (int k = 0; k < mapSize; k++)
					for (int l = 0; l < mapSize; l++) 
				{
				if ((AllowMove[k,l] == 1) && (BoardState[k,l] == -6) )
					print("check" + k + l);
				}
			break;
			}
		}
	}
    void MoveFigure(int h, int v)
    {
        for (int i = 0; i < mapSize; i++)
            for (int j = 0; j < mapSize; j++)
                AllowMove[i, j] = 0;
        switch (BoardState[h, v])
        {
            //White pawn
            case 1:
                if (h == 6)
                {
                    if (BoardState[h - 1, v] == 0)
                        AllowMove[h - 1, v] = 1;
                    if (BoardState[h - 2, v] == 0)
                        AllowMove[h - 2, v] = 1;
                }
					
                if ((0 < h) && (h < 6))
                {
                    if (BoardState[h - 1, v] == 0)
                        AllowMove[h - 1, v] = 1;
                    if ((v > 0) && (v < 7))
                    {
                        if (BoardState[h - 1, v - 1] < 0)
                            AllowMove[h - 1, v - 1] = 1;
                        if (BoardState[h - 1, v + 1] < 0)
                            AllowMove[h - 1, v + 1] = 1;
                    }
                    if (v == 0)
                        if (BoardState[h - 1, v + 1] < 0)
                            AllowMove[h - 1, v + 1] = 1;
                    if (v == 7)
                        if (BoardState[h - 1, v - 1] < 0)
                            AllowMove[h - 1, v - 1] = 1;
				}
                break;
            //White knight
            case 2:
                for (int i = 0; i < mapSize; i++)
                    for (int j = 0; j < mapSize; j++)
                    {
                        if ((((Mathf.Abs(i - h) == 2) && (Mathf.Abs(j - v) == 1)) || ((Mathf.Abs(i - h) == 1) && (Mathf.Abs(j - v) == 2))) && (BoardState[i, j] <= 0))
                            AllowMove[i, j] = 1;
                    }
                break;
            //White bishop
            case 3:
                for (int i = 1; i < mapSize; i++)
                {
                    if ((h + i <= 7) && (v + i <= 7))
                    {
                        if (BoardState[h + i, v + i] > 0)
                            break;
                        AllowMove[h + i, v + i] = 1;
                        if (BoardState[h + i, v + i] < 0)
                            break;
                    }
                }
                for (int i = 1; i < mapSize; i++)
                {
                    if ((h + i <= 7) && (v - i >= 0))
                    {
                        if (BoardState[h + i, v - i] > 0)
                            break;
                        AllowMove[h + i, v - i] = 1;
                        if (BoardState[h + i, v - i] < 0)
                            break;
                    }
                }
                for (int i = 1; i < mapSize; i++)
                {
                    if ((h - i >= 0) && (v + i <= 7))
                    {
                        if (BoardState[h - i, v + i] > 0)
                            break;
                        AllowMove[h - i, v + i] = 1;
                        if (BoardState[h - i, v + i] < 0)
                            break;
                    }
                }
                for (int i = 1; i < mapSize; i++)
                {
                    if ((h - i >= 0) && (v - i >= 0))
                    {
                        if (BoardState[h - i, v - i] > 0)
                            break;
                        AllowMove[h - i, v - i] = 1;
                        if (BoardState[h - i, v - i] < 0)
                            break;
                    }
                }
                break;
            //White rook
            case 4:
                for (int i = 1; i < mapSize; i++)
                {
                    if (h + i <= 7)
                    {
                        if (BoardState[h + i, v] > 0)
                            break;
                        AllowMove[h + i, v] = 1;
                        if (BoardState[h + i, v] < 0)
                            break;
                    }
                }
                for (int i = 1; i < mapSize; i++)
                {
                    if (v - i >= 0)
                    {
                        if (BoardState[h, v - i] > 0)
                            break;
                        AllowMove[h, v - i] = 1;
                        if (BoardState[h, v - i] < 0)
                            break;
                    }
                }
                for (int i = 1; i < mapSize; i++)
                {
                    if (h - i >= 0)
                    {
                        if (BoardState[h - i, v] > 0)
                            break;
                        AllowMove[h - i, v] = 1;
                        if (BoardState[h - i, v] < 0)
                            break;
                    }
                }
                for (int i = 1; i < mapSize; i++)
                {
                    if (v + i <= 7)
                    {
                        if (BoardState[h, v + i] > 0)
                            break;
                        AllowMove[h, v + i] = 1;
                        if (BoardState[h, v + i] < 0)
                            break;
                    }
                }
                break;
            //White queen
            case 5:
                for (int i = 1; i < mapSize; i++)
                {
                    if ((h + i <= 7) && (v + i <= 7))
                    {
                        if (BoardState[h + i, v + i] > 0)
                            break;
                        AllowMove[h + i, v + i] = 1;
                        if (BoardState[h + i, v + i] < 0)
                            break;
                    }
                }
                for (int i = 1; i < mapSize; i++)
                {
                    if ((h + i <= 7) && (v - i >= 0))
                    {
                        if (BoardState[h + i, v - i] > 0)
                            break;
                        AllowMove[h + i, v - i] = 1;
                        if (BoardState[h + i, v - i] < 0)
                            break;
                    }
                }
                for (int i = 1; i < mapSize; i++)
                {
                    if ((h - i >= 0) && (v + i <= 7))
                    {
                        if (BoardState[h - i, v + i] > 0)
                            break;
                        AllowMove[h - i, v + i] = 1;
                        if (BoardState[h - i, v + i] < 0)
                            break;
                    }
                }
                for (int i = 1; i < mapSize; i++)
                {
                    if ((h - i >= 0) && (v - i >= 0))
                    {
                        if (BoardState[h - i, v - i] > 0)
                            break;
                        AllowMove[h - i, v - i] = 1;
                        if (BoardState[h - i, v - i] < 0)
                            break;
                    }
                }
                for (int i = 1; i < mapSize; i++)
                {
                    if (h + i <= 7)
                    {
                        if (BoardState[h + i, v] > 0)
                            break;
                        AllowMove[h + i, v] = 1;
                        if (BoardState[h + i, v] < 0)
                            break;
                    }
                }
                for (int i = 1; i < mapSize; i++)
                {
                    if (v - i >= 0)
                    {
                        if (BoardState[h, v - i] > 0)
                            break;
                        AllowMove[h, v - i] = 1;
                        if (BoardState[h, v - i] < 0)
                            break;
                    }
                }
                for (int i = 1; i < mapSize; i++)
                {
                    if (h - i >= 0)
                    {
                        if (BoardState[h - i, v] > 0)
                            break;
                        AllowMove[h - i, v] = 1;
                        if (BoardState[h - i, v] < 0)
                            break;
                    }
                }
                for (int i = 1; i < mapSize; i++)
                {
                    if (v + i <= 7)
                    {
                        if (BoardState[h, v + i] > 0)
                            break;
                        AllowMove[h, v + i] = 1;
                        if (BoardState[h, v + i] < 0)
                            break;
                    }
                }
                break;
            //White king
            case 6:
                if (h - 1 >= 0)
                    if (BoardState[h - 1, v] <= 0)
                        AllowMove[h - 1, v] = 1;
                if (h + 1 <= 7)
                    if (BoardState[h + 1, v] <= 0)
                        AllowMove[h + 1, v] = 1;
                if (v - 1 >= 0)
                    if (BoardState[h, v - 1] <= 0)
                        AllowMove[h, v - 1] = 1;
                if (v + 1 <= 7)
                    if (BoardState[h, v + 1] <= 0)
                        AllowMove[h, v + 1] = 1;
                if ((h - 1 >= 0) && (v - 1 >= 0))
                    if (BoardState[h - 1, v - 1] <= 0)
                        AllowMove[h - 1, v - 1] = 1;
                if ((h - 1 >= 0) && (v + 1 <= 7))
                    if (BoardState[h - 1, v + 1] <= 0)
                        AllowMove[h - 1, v + 1] = 1;
                if ((h + 1 <= 7) && (v - 1 >= 0))
                    if (BoardState[h + 1, v - 1] <= 0)
                        AllowMove[h + 1, v - 1] = 1;
                if ((h + 1 <= 7) && (v + 1 <= 7))
                    if (BoardState[h + 1, v + 1] <= 0)
                        AllowMove[h + 1, v + 1] = 1;
				if ((h == 7) && (v == 4) && (enable_Castling == true))
					{
						if ((BoardState[h, v+1] == 0) && (BoardState[h, v+2] == 0))
						{
							AllowMove[h,v+2] = 1;
							BoardState[h,v+3] = 0;
							BoardState[h,v+1] = 4;
							enable_Castling = false;
						}
				    }
				if ((h == 7) && (v == 4) && (enable_Castling == true))
				{
				if ((BoardState[h, v-1] == 0) && (BoardState[h, v-2] == 0) && (BoardState[h, v-3] == 0))
					{
						AllowMove[h,v-3] = 1;
						BoardState[h,v-4] = 0;
						BoardState[h,v-2] = 4;
						enable_Castling = false;
					}
				}
				

			break;
			
			//Black pawn
            case -1:
                if (h == 1)
                {
                    if (BoardState[h + 1, v] == 0)
                        AllowMove[h + 1, v] = 1;
                    if (BoardState[h + 2, v] == 0)
                        AllowMove[h + 2, v] = 1;
                }
                if ((h > 1) && (h < 7))
                {
                    if (BoardState[h + 1, v] == 0)
                        AllowMove[h + 1, v] = 1;
                    if ((v > 0) && (v < 7))
                    {
                        if (BoardState[h + 1, v - 1] > 0)
                            AllowMove[h + 1, v - 1] = 1;
                        if (BoardState[h + 1, v + 1] > 0)
                            AllowMove[h + 1, v + 1] = 1;
                    }
                    if (v == 0)
                        if (BoardState[h + 1, v + 1] > 0)
                            AllowMove[h + 1, v + 1] = 1;
                    if (v == 7)
                        if (BoardState[h + 1, v - 1] > 0)
                            AllowMove[h + 1, v - 1] = 1;
                }
                break;
            //Black knight
            case -2:
                for (int i = 0; i < mapSize; i++)
                    for (int j = 0; j < mapSize; j++)
                    {
                        if ((((Mathf.Abs(i - h) == 2) && (Mathf.Abs(j - v) == 1)) || ((Mathf.Abs(i - h) == 1) && (Mathf.Abs(j - v) == 2))) && (BoardState[i, j] >= 0))
                            AllowMove[i, j] = 1;
                    }
                break;
            //Black bishop
            case -3:
                for (int i = 1; i < mapSize; i++)
                {
                    if ((h + i <= 7) && (v + i <= 7))
                    {
                        if (BoardState[h + i, v + i] < 0)
                            break;
                        AllowMove[h + i, v + i] = 1;
                        if (BoardState[h + i, v + i] > 0)
                            break;
                    }
                }
                for (int i = 1; i < mapSize; i++)
                {
                    if ((h + i <= 7) && (v - i >= 0))
                    {
                        if (BoardState[h + i, v - i] < 0)
                            break;
                        AllowMove[h + i, v - i] = 1;
                        if (BoardState[h + i, v - i] > 0)
                            break;
                    }
                }
                for (int i = 1; i < mapSize; i++)
                {
                    if ((h - i >= 0) && (v + i <= 7))
                    {
                        if (BoardState[h - i, v + i] < 0)
                            break;
                        AllowMove[h - i, v + i] = 1;
                        if (BoardState[h - i, v + i] > 0)
                            break;
                    }
                }
                for (int i = 1; i < mapSize; i++)
                {
                    if ((h - i >= 0) && (v - i >= 0))
                    {
                        if (BoardState[h - i, v - i] < 0)
                            break;
                        AllowMove[h - i, v - i] = 1;
                        if (BoardState[h - i, v - i] > 0)
                            break;
                    }
                }
                break;
            //Black rook
            case -4:
                for (int i = 1; i < mapSize; i++)
                {
                    if (h + i <= 7)
                    {
                        if (BoardState[h + i, v] < 0)
                            break;
                        AllowMove[h + i, v] = 1;
                        if (BoardState[h + i, v] > 0)
                            break;
                    }
                }
                for (int i = 1; i < mapSize; i++)
                {
                    if (v - i >= 0)
                    {
                        if (BoardState[h, v - i] < 0)
                            break;
                        AllowMove[h, v - i] = 1;
                        if (BoardState[h, v - i] > 0)
                            break;
                    }
                }
                for (int i = 1; i < mapSize; i++)
                {
                    if (h - i >= 0)
                    {
                        if (BoardState[h - i, v] < 0)
                            break;
                        AllowMove[h - i, v] = 1;
                        if (BoardState[h - i, v] > 0)
                            break;
                    }
                }
                for (int i = 1; i < mapSize; i++)
                {
                    if (v + i <= 7)
                    {
                        if (BoardState[h, v + i] < 0)
                            break;
                        AllowMove[h, v + i] = 1;
                        if (BoardState[h, v + i] > 0)
                            break;
                    }
                }
                break;
            //Black queen
            case -5:
                for (int i = 1; i < mapSize; i++)
                {
                    if ((h + i <= 7) && (v + i <= 7))
                    {
                        if (BoardState[h + i, v + i] < 0)
                            break;
                        AllowMove[h + i, v + i] = 1;
                        if (BoardState[h + i, v + i] > 0)
                            break;
                    }
                }
                for (int i = 1; i < mapSize; i++)
                {
                    if ((h + i <= 7) && (v - i >= 0))
                    {
                        if (BoardState[h + i, v - i] < 0)
                            break;
                        AllowMove[h + i, v - i] = 1;
                        if (BoardState[h + i, v - i] > 0)
                            break;
                    }
                }
                for (int i = 1; i < mapSize; i++)
                {
                    if ((h - i >= 0) && (v + i <= 7))
                    {
                        if (BoardState[h - i, v + i] < 0)
                            break;
                        AllowMove[h - i, v + i] = 1;
                        if (BoardState[h - i, v + i] > 0)
                            break;
                    }
                }
                for (int i = 1; i < mapSize; i++)
                {
                    if ((h - i >= 0) && (v - i >= 0))
                    {
                        if (BoardState[h - i, v - i] < 0)
                            break;
                        AllowMove[h - i, v - i] = 1;
                        if (BoardState[h - i, v - i] > 0)
                            break;
                    }
                }
                for (int i = 1; i < mapSize; i++)
                {
                    if (h + i <= 7)
                    {
                        if (BoardState[h + i, v] < 0)
                            break;
                        AllowMove[h + i, v] = 1;
                        if (BoardState[h + i, v] > 0)
                            break;
                    }
                }
                for (int i = 1; i < mapSize; i++)
                {
                    if (v - i >= 0)
                    {
                        if (BoardState[h, v - i] < 0)
                            break;
                        AllowMove[h, v - i] = 1;
                        if (BoardState[h, v - i] > 0)
                            break;
                    }
                }
                for (int i = 1; i < mapSize; i++)
                {
                    if (h - i >= 0)
                    {
                        if (BoardState[h - i, v] < 0)
                            break;
                        AllowMove[h - i, v] = 1;
                        if (BoardState[h - i, v] > 0)
                            break;
                    }
                }
                for (int i = 1; i < mapSize; i++)
                {
                    if (v + i <= 7)
                    {
                        if (BoardState[h, v + i] < 0)
                            break;
                        AllowMove[h, v + i] = 1;
                        if (BoardState[h, v + i] > 0)
                            break;
                    }
                }
                break;
            //Black king
            case -6:
                if (h - 1 >= 0)
                    if (BoardState[h - 1, v] >= 0)
                        AllowMove[h - 1, v] = 1;
                if (h + 1 <= 7)
                    if (BoardState[h + 1, v] >= 0)
                        AllowMove[h + 1, v] = 1;
                if (v - 1 >= 0)
                    if (BoardState[h, v - 1] >= 0)
                        AllowMove[h, v - 1] = 1;
                if (v + 1 <= 7)
                    if (BoardState[h, v + 1] >= 0)
                        AllowMove[h, v + 1] = 1;
                if ((h - 1 >= 0) && (v - 1 >= 0))
                    if (BoardState[h - 1, v - 1] >= 0)
                        AllowMove[h - 1, v - 1] = 1;
                if ((h - 1 >= 0) && (v + 1 <= 7))
                    if (BoardState[h - 1, v + 1] >= 0)
                        AllowMove[h - 1, v + 1] = 1;
                if ((h + 1 <= 7) && (v - 1 >= 0))
                    if (BoardState[h + 1, v - 1] >= 0)
                        AllowMove[h + 1, v - 1] = 1;
                if ((h + 1 <= 7) && (v + 1 <= 7))
                    if (BoardState[h + 1, v + 1] >= 0)
                        AllowMove[h + 1, v + 1] = 1;

                break;
        }
        // Debug.Log(AllowMove[h - 1, v]);
    }
}